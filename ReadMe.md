Installation

When you never have used SleepWatcher before, the following steps are suggests, assuming that you have unpacked the SleepWatcher 2.2 download on your desktop, i. e. you have the folder ~/Desktop/sleepwatcher_2.2 with this ReadMe as ~/Desktop/sleepwatcher_2.2/ReadMe.rtf.

1. Install the SleepWatcher software:

$ sudo mkdir -p /usr/local/sbin /usr/local/share/man/man8
$ sudo cp ~/Desktop/sleepwatcher_2.2/sleepwatcher /usr/local/sbin
$ sudo cp ~/Desktop/sleepwatcher_2.2/sleepwatcher.8 /usr/local/share/man/man8

2. Read the man page and think about the features of SleepWatcher you want to use:

$ man sleepwatcher

3. Write small shell scripts that perform the actions you want to trigger by SleepWacher and test them.

4. Test your scripts in combination with SleepWatcher, started in the foreground in a Terminal window, e. g.:

$ /usr/local/sbin/sleepwatcher --verbose --sleep /path/to/your/sleepscript --wakeup /path/to/your/wakeupscript ...

5. Put your SleepWatcher command line into a launchd agent configuration file. As an example you can use one of the plists from ~/Desktop/sleepwatcher_2.2/config. For more information about launchd and its configuration, see the man pages for launchd and launchd.plist and http://developer.apple.com/macosx/launchd.html.

6. Copy your launchd agent configuration to /Library/LaunchDaemons or ~/Library/LaunchAgents, depending on whether you need a system wide daemon or a user agent. 

7. Load the launchd agent configuration using launchctl (see examples above and the launchctl man page).
